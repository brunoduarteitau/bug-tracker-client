var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('bug/index', { title: 'Bug Tracker - Bugs' });
});

router.get('/:id', function(req, res, next) {
  res.render('bug/show', { title: 'Bug Tracker - Bug' });
});

router.get('/new', function(req, res, next) {
  res.render('bug/new', { title: 'Bug Tracker - Novo Bug' });
});

router.post('/new', function(req, res, next) {
  res.send(req.body); // Faz a requisição para o servidor
});

router.get('/edit', function(req, res, next) {
  res.render('bug/edit', { title: 'Bug Tracker - Editar Bug' });
});

router.post('/edit', function(req, res, next) {
  res.send(req.body); // Faz a requisição para o servidor
});

module.exports = router;
