var express = require('express');
var request = require('request');
var mongoose = require('mongoose');
var router = express.Router();

router.get('/', function(req, res, next) {
    console.log('## Teste ##');
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log('## Teste ##');
        var kittySchema = new mongoose.Schema({
            name: String
        });

        var Kitten = mongoose.model('Kitten', kittySchema);

        var silence = new Kitten({ name: 'Silence' });
        console.log(silence.name);

        /*
        Kitten.save(function (err, silence) {
            if (err) return console.error(err);
        });

        Kitten.find(function (err, kittens) {
            if (err) return console.error(err);
            console.log(kittens);
        });
        */
    });
});

/*router.get('/:id', function(req, res, next) {
  res.render('project/show', { title: 'Bug Tracker - Projeto' });
});*/

router.get('/new', function(req, res, next) {
  res.render('project/new', { title: 'Bug Tracker - Novo Projeto' });
});

router.post('/new', function(req, res, next) {

  var db = req.db;

  var name = req.body.name;
  var description = req.body.description;

  var collection = db.get('projectcollection');

  collection.insert({
    'name' : name,
    'description' : description
  }, function (err, doc) {
    if (err) {
      // If it failed, return error
      res.send(err);
    } else {
      // And forward to success page
      res.redirect('/project');
    }
  });
});

router.get('/edit', function(req, res, next) {
  res.render('project/edit', { title: 'Bug Tracker - Editar Projeto' });
});

router.post('/edit', function(req, res, next) {
  res.send(req.body); // Faz a requisição para o servidor
});

module.exports = router;
