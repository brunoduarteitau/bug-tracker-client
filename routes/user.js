var express = require('express');
var request = require("request");
var router = express.Router();
var user = require('./../models/users')();

router.get('/', function(req, res, next) {
  user.find(null, function(error, users) {
    if(error) {
      res.render('user/index', { title: 'Bug Tracker - Dashboard', error: error });
    }
    res.render('user/index', { title: 'Bug Tracker - Dashboard', users: users });
  });
});

router.get('/edit', function(req, res, next) {
  res.render('user/edit', { title: 'Bug Tracker - Editar usuário' });
});

router.post('/edit', function(req, res, next) {
  res.send(req.body); // Faz a requisição para o servidor
});

router.get('/signup', function(req, res, next) {
  res.render('user/signup', { title: 'Bug Tracker - Cadastre-se' });
});

router.post('/signup', function(req, res, next) {
  var body = req.body;
  user.create(body, function(error, user) {
    if(error) {
      res.render('user/signup', { title: 'Bug Tracker - Cadastre-se', error: error });
    }
    res.redirect('/');
  });
});

router.get('/login', function(req, res, next) {
  res.render('user/login', { title: 'Bug Tracker - Entrar' });
});

router.post('/login', function(req, res, next) {
  res.send(req.body); // Faz a requisição para o servidor
});

router.get('/logout', function(req, res, next) {
  // Faz a requisição para o servidor
});

module.exports = router;
