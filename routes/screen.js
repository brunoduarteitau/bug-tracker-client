var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('screen/index', { title: 'Bug Tracker - Telas' });
});

router.get('/:id', function(req, res, next) {
  res.render('screen/show', { title: 'Bug Tracker - Tela' });
});

router.get('/new', function(req, res, next) {
  res.render('screen/new', { title: 'Bug Tracker - Novo Tela' });
});

router.post('/new', function(req, res, next) {
  res.send(req.body); // Faz a requisição para o servidor
});

router.get('/edit', function(req, res, next) {
  res.render('screen/edit', { title: 'Bug Tracker - Editar Tela' });
});

router.post('/edit', function(req, res, next) {
  res.send(req.body); // Faz a requisição para o servidor
});

module.exports = router;
