var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('flow/index', { title: 'Bug Tracker - Fluxos' });
});

router.get('/:id', function(req, res, next) {
  res.render('flow/show', { title: 'Bug Tracker - Fluxo' });
});

router.get('/new', function(req, res, next) {
  res.render('flow/new', { title: 'Bug Tracker - Novo Fluxo' });
});

router.post('/new', function(req, res, next) {
  res.send(req.body); // Faz a requisição para o servidor
});

router.get('/edit', function(req, res, next) {
  res.render('flow/edit', { title: 'Bug Tracker - Editar Fluxo' });
});

router.post('/edit', function(req, res, next) {
  res.send(req.body); // Faz a requisição para o servidor
});

module.exports = router;
