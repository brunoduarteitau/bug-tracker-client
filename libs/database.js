var mongoose = require('mongoose');
var db;

module.exports = function() {
    if (!db) {
        db = mongoose.connect('mongodb://mongo:27017/sourcelair', { useNewUrlParser: true });
    }
    return db;
}
