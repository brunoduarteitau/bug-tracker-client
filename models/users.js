module.exports = function() {
    var db = require('./../libs/database')();
    var mongoose = require('mongoose');

    var user = mongoose.Schema({
        name: String,
        email: String,
        password: String
    });

    return mongoose.model('users', user);
}
